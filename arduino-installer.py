#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""
INSTALADOR para arduino ide em sistemas Linux e Windows.

USE:
  ./arduino-installer.py --install


  ./arduino-installer.py --uninstall
"""


import sys
import os
import shutil
import argparse
from getpass import getuser
from pathlib import Path

# Proteção contra root em sistemas posix:
if os.name == 'posix':
	if os.geteuid() == 0:
		print('Você não pode ser o "root".')
		sys.exit()


__version__ = '0.1.1'
__appname__ = 'arduino-installer'
_script = os.path.realpath(__file__)
dir_of_project = os.path.dirname(_script)
DIR_HOME = os.path.abspath(os.path.join(Path().home()))
DIR_CACHE = os.path.abspath(os.path.join(DIR_HOME, '.cache', __appname__))


if sys.platform == 'linux':
	# arduino-nightly-linux64.tar.xz
	CACHE_FILE = os.path.abspath(os.path.join(DIR_CACHE, 'arduino-nightly-linux64.tar.xz'))
elif sys.platform == 'win32':
	CACHE_FILE = os.path.abspath(os.path.join(DIR_CACHE, 'arduino.exe'))
else:
	print('Seu sistema não é suportado.')
	sys.exit(1)



parser = argparse.ArgumentParser(
			description='Instala o Arduino IDE em sistemas Linux e Windows.')


def downloader(url: str, output_file: str) -> bool:

	if os.path.isfile(output_file):
		print(f'Arquivo encontrado ... {output_file}')
		return True

	__downloader_lib = None
	try:
		import wget
	except:
		import urllib.request
	else:
		__downloader_lib='python_wget'


	print(f'Baixando ... {url}')
	if __downloader_lib == 'python_wget':
		try:
			wget.download(url, output_file)
		except Exception as e:
			print() 
			print(e)
			return False
		else:
			print()
			return True
	else:
		try:
			urllib.request.urlretrieve(url, output_file)
		except Exception as e:
			print(e)
			return False
		else:
			return True


class LinuxInstaller(object):
	def __init__(self):
		self.url = 'https://downloads.arduino.cc/arduino-nightly-linux64.tar.xz'
		self.script_linux_installer = os.path.abspath(os.path.join(dir_of_project, 'linux-installer.sh'))

	def download(self) -> bool:
		return downloader(self.url, CACHE_FILE)

	def install(self) -> bool:
		os.system(f'bash {self.script_linux_installer} install --file={CACHE_FILE}')
		return True

	def uninstall(self) -> bool:
		os.system(f'bash {self.script_linux_installer} uninstall')
		return True

	def unpack(self) -> bool:
		pass



class WindowsInstaller(object):
	def __init__(self):
		self.url = ''

	def install(self) -> bool:
		pass

	def uninstall(self) -> bool:
		pass

	def download(self) -> bool:
		pass 


class InstallArduinoIde(object):
	"""docstring for InstallArduinoIde"""
	def __init__(self, installer: object):
		super().__init__()
		self._installer: object = installer
		
	def download(self) -> bool:
		return self._installer.download()

	def install(self) -> bool:
		return self._installer.install()

	def uninstall(self) -> bool:
		return self._installer.uninstall()



def main():

	parser.add_argument(
		'-v', '--version',
		action='version',
		version=__version__,
	)

	parser.add_argument(
		'-u', '--uninstall',
		action='store_true',
		dest='uninstall_arduino_ide',
		help='Desinstala o Arduino IDE.'
	)


	parser.add_argument(
		'-i', '--installl',
		action='store_true',
		dest='install_arduino_ide',
		help='Instala o Arduino IDE.'
	)

	args = parser.parse_args()


	if sys.platform == 'linux':
		__installer = LinuxInstaller()
	elif sys.platform == 'win32':
		__installer = 'win32'
	else:
		print('Seu sistema não é suportado.')
		sys.exit(1)

	installer = InstallArduinoIde(__installer)

	if args.install_arduino_ide:

		if not os.path.isdir(DIR_CACHE):
			try:
				os.makedirs(DIR_CACHE)
			except Exception as e:
				print(e)
				sys.exit(1)
		
		installer.download()
		installer.install()

	elif args.uninstall_arduino_ide:
		installer.uninstall()



if __name__ == '__main__':
	main()