#!/usr/bin/env bash
#
# https://gitlab.com/bschaves/bash_libs/-/raw/v0.1.0/setup.sh?inline=false
#
#

version='0.1.1'

clear

readonly URL_ARDUINO='https://downloads.arduino.cc/arduino-nightly-linux64.tar.xz'
readonly DESTINATION_DIR_ARDUINO=~/.local/share/arduino-ide
readonly DESTINATION_FILE_DESKTOP=~/.local/share/applications/arduino-arduinoide.desktop
readonly DESTINATION_SCRIPT=~/.local/share/arduino-ide/arduino
readonly DESTINATION_LINK=~/.local/bin/arduino
readonly DESTINATION_ICON=~/.local/share/icons/arduino.png

mkdir -p ~/.local/bin
mkdir -p ~/.local/share/applications
mkdir -p ~/.local/share/icons



function install_bash_libs()
{
	# Módulos bash
	local url_setup_file='https://gitlab.com/bschaves/bash_libs/-/raw/v0.1.0/setup.sh?inline=false'
	local temp_setup_file=$(mktemp -u)

	echo -e "Baixando instalador bash_libs."
	curl -sSL "$url_setup_file" -o "$temp_setup_file" || return 1

	chmod +x "$temp_setup_file"
	"$temp_setup_file"
}

function create_desktop_entry()
{
	echo '[Desktop Entry]' > "$DESTINATION_FILE_DESKTOP"

	{
	echo "Type=Application";
	echo "Name=Arduino IDE";
	echo "GenericName=Arduino IDE"
	echo "Comment=Open-source electronics prototyping platform";
	echo "Exec=$DESTINATION_DIR_ARDUINO/arduino"
	echo -e "Icon=$DESTINATION_ICON";
	echo "Terminal=false;"
	echo "Categories=Development;IDE;Electronics;";
	echo "MimeType=text/x-arduino;";
	echo "Keywords=embedded electronics;electronics;avr;microcontroller;"
	echo "StartupWMClass=processing-app-Base;"
	} >> "$DESTINATION_FILE_DESKTOP"

}


function install_arduino_ide()
{
	if [[ -d "$DESTINATION_DIR_ARDUINO" ]]; then	
		echo -e "Arduino IDE já instalado."
		return 0
	fi

	#install_bash_libs 
	source ~/.bash_libsrc
	[[ -z $SYSTEM ]] && {
		echo -e "ERRO ... bash_libs não está instalado."
		return 1
	}

	source "$SYSTEM"
	source "$REQUESTS"

	#local __arduino_tmp_file=$(mktemp -u)
	local __arduino_tmp_file='/tmp/arduino-nightly-linux64.tar.xz'
	local __tmp_dir=$(mktemp -d)

	mkdir -p "$DESTINATION_DIR_ARDUINO"
	downloader "$URL_ARDUINO" "$__arduino_tmp_file"
	unpack_archive "$__arduino_tmp_file" "$__tmp_dir"
	cd $__tmp_dir
	echo -e "Copiando arquivos."
	mv $(ls -d arduino-nightly*) arduino
	cd arduino
	cp -R * "$DESTINATION_DIR_ARDUINO"/
	cp -R "$DESTINATION_DIR_ARDUINO"/lib/icons/128x128/apps/arduino.png "$DESTINATION_ICON"
	ln -sf "$DESTINATION_SCRIPT" "$DESTINATION_LINK"
	cd "$DESTINATION_DIR_ARDUINO"
	./install.sh
	create_desktop_entry


	rm -rf "$__tmp_dir"
	#rm -rf "$__arduino_tmp_file"
}

function uninstall_arduino()
{
	echo -e "Deletando ... $DESTINATION_ICON"; rm -rf "$DESTINATION_ICON"
	echo -e "Deletando ... $DESTINATION_FILE_DESKTOP"; rm -rf "$DESTINATION_FILE_DESKTOP"
	echo -e "Deletando ... $DESTINATION_LINK"; rm -rf "$DESTINATION_LINK"
	echo -e "Deletando ... $DESTINATION_DIR_ARDUINO"; rm -rf "$DESTINATION_DIR_ARDUINO"
}

function main()
{
	[[ -z $1 ]] && install_arduino_ide

	while [[ $1 ]]; do
		case "$1" in 
			install) install_arduino_ide; break;;
			uninstall) uninstall_arduino; break;;
			*) install_arduino_ide; break;;
		esac
	done
}


main "$@"
