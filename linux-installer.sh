#!/usr/bin/env bash
#
# https://gitlab.com/bschaves/bash_libs/-/raw/v0.1.0/setup.sh?inline=false
#
# USO:
#
# ./linux-installer install --file=arduino-nightly-linux64.tar.xz
# ./linux-installer uninstall
#
#
#

version='0.1.1'

readonly DESTINATION_DIR_ARDUINO=~/.local/share/arduino-ide
readonly DESTINATION_FILE_DESKTOP=~/.local/share/applications/arduino-arduinoide.desktop
readonly DESTINATION_SCRIPT=~/.local/share/arduino-ide/arduino
readonly DESTINATION_LINK=~/.local/bin/arduino
readonly DESTINATION_ICON=~/.local/share/icons/arduino.png
#readonly ARDUINO_CACHE_FILE=~/.cache/$__appname__/arduino-nightly-linux64.tar.xz
ARDUINO_CACHE_FILE='None'

mkdir -p ~/.local/bin
mkdir -p ~/.local/share/applications
mkdir -p ~/.local/share/icons



function create_desktop_entry()
{
	echo '[Desktop Entry]' > "$DESTINATION_FILE_DESKTOP"

	{
	echo "Type=Application";
	echo "Name=Arduino IDE";
	echo "GenericName=Arduino IDE"
	echo "Comment=Open-source electronics prototyping platform";
	echo "Exec=$DESTINATION_DIR_ARDUINO/arduino"
	echo -e "Icon=$DESTINATION_ICON";
	echo "Terminal=false;"
	echo "Categories=Development;IDE;Electronics;";
	echo "MimeType=text/x-arduino;";
	echo "Keywords=embedded electronics;electronics;avr;microcontroller;"
	echo "StartupWMClass=processing-app-Base;"
	} >> "$DESTINATION_FILE_DESKTOP"

}


function install_arduino_ide()
{
	if [[ -d "$DESTINATION_DIR_ARDUINO" ]]; then	
		echo -e "Arduino IDE já instalado."
		return 0
	fi


	if [[ "$ARDUINO_CACHE_FILE" == 'None' ]]; then
		echo -e "install_arduino_ide ERRO: nenhum arquivo informado na opção --file"
		return 1
	fi


	local __arduino_tmp_file="$ARDUINO_CACHE_FILE"
	local __tmp_dir=$(mktemp -d)

	mkdir -p "$DESTINATION_DIR_ARDUINO"
	cd $__tmp_dir
	echo -e "Descompactando ... $ARDUINO_CACHE_FILE"
	tar -Jxf "$ARDUINO_CACHE_FILE" -C "$__tmp_dir" 1> /dev/null  

	echo -e "Copiando arquivos."
	mv $(ls -d arduino-nightly*) arduino
	cd arduino
	cp -R * "$DESTINATION_DIR_ARDUINO"/
	cp -R "$DESTINATION_DIR_ARDUINO"/lib/icons/128x128/apps/arduino.png "$DESTINATION_ICON"
	ln -sf "$DESTINATION_SCRIPT" "$DESTINATION_LINK"
	cd "$DESTINATION_DIR_ARDUINO"
	./install.sh
	create_desktop_entry

	rm -rf "$__tmp_dir"
	#rm -rf "$__arduino_tmp_file"
}

function uninstall_arduino()
{
	echo -e "Deletando ... $DESTINATION_ICON"; rm -rf "$DESTINATION_ICON"
	echo -e "Deletando ... $DESTINATION_FILE_DESKTOP"; rm -rf "$DESTINATION_FILE_DESKTOP"
	echo -e "Deletando ... $DESTINATION_LINK"; rm -rf "$DESTINATION_LINK"
	echo -e "Deletando ... $DESTINATION_DIR_ARDUINO"; rm -rf "$DESTINATION_DIR_ARDUINO"
}

function main()
{
	[[ -z $1 ]] && return 0

	for ARG in "${@}"; do

		# Obter o caminho do arquivo passado como argumento em --file=/path/to/file.
		if [[ "${ARG:0:7}" == '--file=' ]]; then
			ARDUINO_CACHE_FILE=$(echo -e $ARG | sed 's/--file=//g')
		fi
	done

	

	while [[ $1 ]]; do
		case "$1" in 
			install) install_arduino_ide; break;;
			uninstall) uninstall_arduino; break;;
			*) install_arduino_ide; break;;
		esac
	done
}


main "$@"
